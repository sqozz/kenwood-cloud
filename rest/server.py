import requests
import bottle
import json

from bottle import Bottle, route, run, template, response, request
from bottle.ext.websocket import GeventWebSocketServer
from bottle.ext.websocket import websocket

import threading, queue

BASE = Bottle()

@BASE.route('/devices/hello')
def hello():
    hello_data = {
        "action": "connect",
        "data": {
            "url": "wss://192.168.0.1/ws/foobar"
        }
    }
    return hello_data

@BASE.route('/devices/firmware')
def firmware():
    firmware_data = {
        "action": "none"
    }
    return firmware_data

@BASE.route('/devices/pair-intent')
def pair():
    pair_data = {
        "acknowledged": True
    }
    return pair_data

@BASE.route('/<path:path>')
def index(path):
    print("\"{}\" path is unknown and not implemented".format(path))

bottle.run(
    app=BASE,
    host='127.0.0.1',
    port='8082',
    server=GeventWebSocketServer,
    reloader=1,
    debug=1,
)
