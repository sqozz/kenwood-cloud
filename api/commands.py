import json

class Command():
    def __new__(cls, *args, **kwargs):
        if cls is Command:
            available_commands = sorted(list(map(lambda x: x.__name__, Command.__subclasses__())))
            print("Supported commands:")
            print("\n".join(available_commands))
            raise TypeError(f"only children of '{cls.__name__}' may be instantiated.")
        return super(Command, cls).__new__(cls)

    def __init__(self, *args, **kwargs):
        self.settings = {}
        self.settings.update(kwargs)

    def __str__(self):
        return json.dumps(self.json)

    @property
    def json(self):
        json_data = {"command": self.cmd_name}
        if len(self.settings) > 0:
            json_data["settings"] = self.settings
        return json_data

    @property
    def cmd_name(self):
        cmd_name = type(self).__name__
        camel_name = cmd_name[0].lower() + cmd_name[1:]
        return camel_name


class GetInfo(Command):
    def __init__(self):
        super().__init__()

class SetProgramme(Command):
    def __init__(self, time, temperature, speed):
        if time < 0 or time > 28800:
            raise ValueError("time argument must be between 0 and 28800")
        if temperature < 20 or temperature > 180:
            raise ValueError("temperature argument must be between 20 and 180")
        if speed < 0 or speed > 12:
            raise ValueError("speed argument must be between 0 and 12")
        super().__init__(time=time, temperature=temperature, speed=speed)

class SubscribeWeightUpdates(Command):
    def __init__(self, subscribe=True):
        super().__init__(subscribe=bool(subscribe))

class CancelProgramme(Command):
    def __init__(self):
        super().__init__()
