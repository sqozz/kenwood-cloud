import pprint

class KenwoodMachine():
    def __init__(self):
        self.bowlTemperature = None
        self.heaterTemperature = None
        self.speedSetting = None
        self.remainingTime = None
        self.motorTemperature = None
        self.motorOn = None
        self.hot = None
        self.heating = None
        self.cooking = None
        self.keepWarm = None
        self.remoteMode = None
        self.machineActive = None
        self.localMode = None
        self.temperatureSetting = None
        self.HSHTActive = None
        self.machineInfo = {}

    def __str__(self):
        machine_state = {}
        attributes = list(filter(lambda x: not x.startswith("_"), list(dir(self))))
        for attribute in attributes:
            machine_state.update({attribute: getattr(self, attribute)})
        return pprint.pformat(machine_state, indent=4)
