# kenwood-cloud

A drop-in, privacy friendly replacement for the Kenwood cloud services

## Setup

1. Setup nginx to serve as reverse proxy and ssl endpoint for the services
    1. A template can be found in `extras/nginx.conf`
2. Generate a self-signed certificate for the domain `api.getdrop.io`
3. Setup the DNS of your network to point api.getdrop.io to the IP where these services are running on
    1. A dnsmasq example can be found in `extras/hosts_overwrite`. Add it with `addn-hosts=extras/hosts_overwrite` to your dnsmasq config
4. Start the rest server: `python rest/server.py`
5. Start the websocket server: `PYTHONPATH="$(pwd)" python websocket/server.py`

## Running it

If you setup everything correctly and start up your Kenwood machine you should see a request on the rest server at the `/devices/hello` endpoint. Shortly after this request your websocket server will establish a session with your machine. If everything was successful you can now query the websocket server for all your machine details:

```
» curl http://localhost:8083/status
{   'HSHTActive': False,
    'bowlTemperature': 23,
    'cooking': False,
    'heaterTemperature': 22,
    'heating': False,
    'hot': False,
    'keepWarm': False,
    'localMode': False,
    'machineActive': False,
    'machineInfo': {   'Api': {'env': 'Production', 'version': '<redacted>'},
                       'Appliance': {   'name': 'Kenwood Appliance Library',
                                        'version': '<redacted>'},
                       'Appliance id': 209,
                       'ESP32': {   'cores': <redacted>,
                                    'esp-idf': '<redacted>',
                                    'model': <redacted>,
                                    'revision': <redacted>},
                       'Jansson': '<redacted>',
                       'Kenwood IoT firmware': '<redacted>',
                       'Mac': '<redacted>',
                       'Mongoose': '<redacted>',
                       'Restricted': True,
                       'uC': {   'codeVer': '<redacted>',
                                 'langVer': '<redacted>',
                                 'prodType': '<redacted>',
                                 'protocolVer': '<redacted>'}},
    'motorOn': False,
    'motorTemperature': 25,
    'remainingTime': 0,
    'remoteMode': False,
    'speedSetting': 0,
    'standby': True,
    'temperatureSetting': 0}
```