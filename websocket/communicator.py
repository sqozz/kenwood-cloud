import threading, queue
import json

class DeviceCommunicator():
    def __init__(self, machine):
        self.ws = None
        self.machine = machine
        self.sender_thread = threading.Thread(target=self.sender, args=(self,), daemon=True)
        self.sending_queue = queue.Queue()
        self.send_message('{"command":"getInfo"}')
        self.sender_thread.start()

    def start_ws(self, ws):
        self.ws = ws
        while True:
            if not self.ws.closed:
                message = self.ws.receive()
                if message is not None:
                    self.parse_message(message)
            else:
                break

    def send_message(self, message):
        self.sending_queue.put(message)

    def sender(self, args):
        while True:
            if self.sending_queue.qsize() > 0 and self.ws != None and not self.ws.closed:
                print("Sending message to device")
                self.ws.send(self.sending_queue.get())

    def parse_message(self, message_str):
        message = json.loads(message_str)
        if "type" in message.keys():
            msg_type = message["type"]
            if msg_type == "status":
                dev_data = message["data"]
                for k, v in dev_data.items():
                    setattr(self.machine, k, v)
        elif "acknowledged" in message.keys():
            if message["acknowledged"] == "getInfo":
                info_data = message["result"]
                for k, v in info_data.items():
                    self.machine.machineInfo.update({k: v})
            elif message["acknowledged"] == "subscribeWeightUpdates":
                print("SubscribeWeightUpdates result: {}".format(message["result"]))
                print(message)
            elif message["acknowledged"] == "setProgramme":
                print("Program set result: {}".format(message["result"]))
                print(message)
            elif message["acknowledged"] == "cancelProgramme":
                print("Program cancel result: {}".format(message["result"]))
                print(message)
            else:
                print("Unknown acknowledgement:")
                print(message)
        else:
            print("Cannot parse unknown message:")
            print(message)
