import requests
import bottle
import json

from bottle import Bottle, route, run, template, response, request
from bottle.ext.websocket import GeventWebSocketServer
from bottle.ext.websocket import websocket

from api.machine import KenwoodMachine
from api.commands import *
from communicator import DeviceCommunicator

if __name__ == "__main__":
    BASE = Bottle()
    machine = KenwoodMachine()
    dc = DeviceCommunicator(machine)

    @BASE.get('/ws/<path:path>', apply=[websocket])
    def new_device_connection(ws, path):
        print("Trying to establish websocket connection to device")
        dc.start_ws(ws)

    @BASE.get('/command/getinfo')
    def getInfo():
        dc.sending_queue.put(str(GetInfo()))

    @BASE.get('/command/setprogramme')
    def setProgramme():
        settings = {}
        params = dict(request.query)
        time = params.get("time", 0)
        temperature = params.get("temperature", 0)
        speed = params.get("speed", 0)
        dc.sending_queue.put(str(SetProgramme(int(time), int(temperature), int(speed))))

    @BASE.get('/command/subscribeweightupdates')
    def cancelProgramme():
        dc.sending_queue.put(str(SubscribeWeightUpdates(True)))

    @BASE.get('/command/unsubscribeweightupdates')
    def cancelProgramme():
        dc.sending_queue.put(str(SubscribeWeightUpdates(False)))

    @BASE.get('/command/cancelprogramme')
    def cancelProgramme():
        dc.sending_queue.put(str(CancelProgramme()))

    @BASE.get('/status')
    def get_status():
        return str(machine)

    @BASE.get('/pdb')
    def start_pdb():
        pdb.set_trace()

    bottle.run(
        app=BASE,
        host='127.0.0.1',
        port='8083',
        server=GeventWebSocketServer,
        reloader=1,
        debug=1,
    )
